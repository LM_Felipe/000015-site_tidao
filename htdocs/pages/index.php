﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tidão Telas</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- DataTables Responsive CSS --> 
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Export button styles -->
    <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            
            <div w3-include-html="titulo.html"></div>

            <div w3-include-html="menu.html"></div>
            
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
        
            <!-- Conteudo aqui -->
            <script src="../vendor/w3/w3.js"></script>

            <div w3-include-html="tabela.html"></div>

            <script>
                w3.includeHTML();
            </script>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Script tables export buttons -->
    <script src="../vendor/datatables-plugins/dataTables.buttons.min.js"></script>
    <script src="../vendor/datatables-plugins/buttons.flash.min.js"></script>
    <script src="../vendor/datatables-plugins/jszip.min.js"></script>
    <script src="../vendor/datatables-plugins/pdfmake.min.js"></script>
    <script src="../vendor/datatables-plugins/vfs_fonts.js"></script>
    <script src="../vendor/datatables-plugins/buttons.html5.min.js"></script>
    <script src="../vendor/datatables-plugins/buttons.print.min.js"></script>

    <script src="../data/tabela_3.py"></script>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true,
            dom: 'Bfrtlip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            data: dataSet,
            columns: [
                { title: "Código" },
                { title: "Descrição" },
 { title: "Referência" },
                { title: "Caixa" , "width": "5%"},
                { title: "Estoque" },
                { title: "Total" },
                { title: "Abastecimento", "width": "1%"}
, { title: "Cod. Barras"}
             ],
            "columnDefs": [
            {
                "targets":  [3,4,5],
                "visible": false,
                "searchable": false
            }
        ]
        });
    });
    </script>

</body>

</html>
