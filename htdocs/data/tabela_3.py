#! C:/Python27/python.exe
# -*- coding: UTF-8 -*-

import cgi
import pyodbc
import time
import datetime
import unicodedata

def conect(driver,server,db,usuario,senha):
	try:
		cnxn = pyodbc.connect('DRIVER={'+driver+'};SERVER='+server+';DATABASE='+db+';UID='+usuario+';PWD='+senha)
		return cnxn
	except:
		raise

def remove_accents(input_str):
	nfkd_form = unicodedata.normalize('NFKD', input_str)
	return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

formData = cgi.FieldStorage()

print "Content-type: application/javascript\n"

def print_JSON_DB (cnxn,string):
	cursor = cnxn.cursor()
	cursor.execute(string)
	rows = cursor.fetchall()
	i = 0
	first = True
	print"var dataSet = ["
	for row in rows:
		if first:
			first = False
			print "["
		else:
			print ",["

		print "\"" + str(row.Codigo).strip() + "\","
		desc = remove_accents(row.Descricao).strip().replace("\"","" )
		print "\"" + desc.encode('ascii', 'replace') + "\","

                referencia = remove_accents(row.Referencia).strip().replace("\"","" )
		print "\"" + referencia.encode('ascii', 'replace') + "\","
		
		print "\"" + str(row.Caixa).strip() + "\","
		print "\"" + str(row.Estoque).strip() + "\","
		print "\"" + str(row.Total).strip() + "\","
		print "\"" + str(row.Abastecimento).strip() + "\","
		#print "\"" + remove_accents(row.Unidade).strip() + "\","
		print "\"" + str(row.Cod_Barras).strip() + "\""
		print "]"
	print "];"

cnx = conect("SQL Server",
			 "DESKTOP-J2GR551\SQLTRANSELEVADOR",
			 "Caixas",
			 "sa",
			 "logix16")

querry = """SELECT transelevador_produto.codigo AS Codigo,transelevador_produto.descricao AS Descricao,
transelevador_caixadet.estoque AS Estoque,
transelevador_prodxcaixa.qtdade AS Total,
transelevador_prodxcaixa.qtdade - transelevador_caixadet.estoque AS Abastecimento,
transelevador_caixa.numero AS Caixa,transelevador_caixadet.estoque AS Estoque,transelevador_produto.unidade AS Unidade,
transelevador_produto.cod_barra AS Cod_Barras,transelevador_produto.peso_liquido AS Peso_Liquido,
transelevador_produto.peso_bruto AS Peso_Bruto,transelevador_produto.largura AS Largura,
transelevador_produto.altura AS Altura,transelevador_produto.comprimento AS Comprimento,
transelevador_produto.referencia AS Referencia,
transelevador_produto.empilhamento AS Empilhamento,transelevador_produto.tipo_caixa AS Tipo_Caixa
FROM transelevador_produto 
LEFT JOIN transelevador_caixadet ON transelevador_produto.id_produto = transelevador_caixadet.id_produto
LEFT JOIN transelevador_caixa ON transelevador_caixadet.id_caixa = transelevador_caixa.id_caixa
INNER JOIN transelevador_prodxcaixa ON transelevador_prodxcaixa.id_produto = transelevador_produto.id_produto
ORDER BY transelevador_produto.descricao  ASC,transelevador_caixadet.id_caixa  ASC"""

print_JSON_DB(cnx,querry)
